package com.app.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.app.bean.Doctor;
import com.app.bean.Patient;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;
import com.app.service.IPatient;
import com.app.service.PatientImpl;


/**
 * Servlet implementation class AvailabilityServlet
 */
@WebServlet("/availabilityservlet")
public class AvailabilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AvailabilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		int id=Integer.parseInt(request.getParameter("doctorid"));
		String docName=request.getParameter("doctorname");
		String day=request.getParameter("day");
		String time=request.getParameter("time");
		IPatient patient=new PatientImpl();
		boolean flag=patient.checkAvailabilityInDB(id,time,day);
		System.out.println(flag);
		if(flag){
			
			HttpSession session = request.getSession();
			System.out.println("abc");
			Patient patient1 =(Patient)session.getAttribute("myname");
			if(patient1!=null){
				
				IPatient patientImpl = new PatientImpl();
				patientImpl.bookAppointment(patient1.getName(), docName, time, day);
				IDoctor doctorImpl =new DoctorImpl();
				doctorImpl.updateAvailability(id, time, day);
				request.setAttribute("pname", patient1.getName());
				request.setAttribute("docid", id);
				request.setAttribute("dName", docName);
				request.setAttribute("btime", time);
				request.setAttribute("bday", day);
				RequestDispatcher rd=request.getRequestDispatcher("confirm.jsp");
				rd.forward(request, response);
			}else{
				request.setAttribute("docid", id);
				request.setAttribute("docname", docName);
				request.setAttribute("time", time);
				request.setAttribute("day", day);
				System.out.println("xyz");
				RequestDispatcher rd=request.getRequestDispatcher("visitor.jsp");
				rd.include(request, response);
			}
			
		}
		else{
			out.println("<html><body><script>alert('Slot Not Available')</script></body></html>");
			IDoctor doctorImpl = new DoctorImpl();
			Doctor doctor = doctorImpl.getDoctorId(id);
			request.setAttribute("mydoc", doctor);
			RequestDispatcher rd=request.getRequestDispatcher("viewDoctor.jsp");
			rd.include(request, response);
		}
	}

}
