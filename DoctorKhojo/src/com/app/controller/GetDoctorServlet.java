package com.app.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.bean.Doctor;
import com.app.bean.Patient;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

/**
 * Servlet implementation class GetDoctorServlet
 */
@WebServlet("/getDoctorServlet")
public class GetDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDoctorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String location=request.getParameter("location");
		String specialization=request.getParameter("specialization");
		IDoctor doctor=new DoctorImpl();
		List<Doctor>doctorlist=doctor.getDoctor(location,specialization);
		request.setAttribute("listDoctors", doctorlist);
		RequestDispatcher rd = request.getRequestDispatcher("doctor.jsp");
		rd.include(request, response);
				
	}

}
