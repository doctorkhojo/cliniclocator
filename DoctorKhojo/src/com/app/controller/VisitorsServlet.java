package com.app.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.DoctorImpl;
import com.app.service.IDoctor;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

/**
 * Servlet implementation class VisitorsServlet
 */
@WebServlet("/visitorsServlet")
public class VisitorsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitorsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean flag=false;
		String pName= request.getParameter("name");
		String dName= request.getParameter("bookdocname");
		System.out.println(dName);
		String bDay= request.getParameter("bookday");
		String bTime= request.getParameter("booktime");
		System.out.println(request.getParameter("bookdocid"));
		int dId=Integer.parseInt(request.getParameter("bookdocid"));
		request.setAttribute("pname", pName);
		request.setAttribute("dName", dName);
		request.setAttribute("bday", bDay);
		request.setAttribute("btime", bTime);
		
		IPatient patientImpl = new PatientImpl();
		flag =patientImpl.bookAppointment(pName, dName, bTime, bDay);
		IDoctor doctorImpl =new DoctorImpl();
		doctorImpl.updateAvailability(dId, bTime, bDay);
		
		RequestDispatcher rd = request.getRequestDispatcher("confirm.jsp");
		rd.forward(request, response);
	
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
