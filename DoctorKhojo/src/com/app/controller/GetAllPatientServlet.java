package com.app.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.bean.Doctor;
import com.app.bean.Patient;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

/**
 * Servlet implementation class GetAllPatientServlet
 */
@WebServlet("/getAllPatientServlet")
public class GetAllPatientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAllPatientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IPatient patientImpl=new PatientImpl();
		List<Patient> patientList=patientImpl.getAllPatientDB();
		request.setAttribute("listPatients", patientList);
		RequestDispatcher rd = request.getRequestDispatcher("getPatient.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
