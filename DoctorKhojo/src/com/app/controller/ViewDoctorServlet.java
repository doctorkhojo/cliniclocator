package com.app.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.bean.Doctor;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;

/**
 * Servlet implementation class ViewDoctorServlet
 */
@WebServlet("/viewDoctorServlet")
public class ViewDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewDoctorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("doctor"));
		IDoctor doctorImpl=new DoctorImpl();
		Doctor doctor=doctorImpl.getDoctorId(id);
		request.setAttribute("mydoc", doctor);
		RequestDispatcher rd=request.getRequestDispatcher("viewDoctor.jsp");
		rd.forward(request, response);
		//doctor.viewDoctor();
	}

}
