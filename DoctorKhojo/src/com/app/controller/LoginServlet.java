package com.app.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.app.bean.Patient;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		/*if(name.equalsIgnoreCase("admin")&&password.equalsIgnoreCase("admin")){
			HttpSession session = request.getSession();
			session.setAttribute("myname", "Admin");
			System.out.println("Hello Admin");
			RequestDispatcher rd=request.getRequestDispatcher("admin.jsp");
			rd.forward(request, response);
		}else{*/
		IPatient ref=new PatientImpl();
		Patient patient=new Patient();
		patient=ref.validatePatient(name,password);
		if(patient!=null){
			if(patient.getName().equals("admin")){
				HttpSession session = request.getSession();
				session.setAttribute("myname", patient);
				RequestDispatcher rd=request.getRequestDispatcher("admin.jsp");
				rd.forward(request, response);
			}else{
			//HttpSession session = request.getSession(false);
			HttpSession session = request.getSession();
			session.setAttribute("myname", patient);
			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
			}
		}else{
			HttpSession session = request.getSession();
			session.setAttribute("myname", patient);
			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		}
	}
	}

