package com.app.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.bean.Doctor;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;

/**
 * Servlet implementation class AddDoctorServlet
 */
@WebServlet("/addDoctorServlet")
public class AddDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDoctorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String name=request.getParameter("name");
		String speciality=request.getParameter("specialization");
		String location=request.getParameter("location");
		int age=Integer.parseInt(request.getParameter("age"));
		String gender=request.getParameter("gender");
		//int id=Integer.parseInt(request.getParameter("id"));
		Doctor doctor=new Doctor();
		//doctor.setDocID(id);
		doctor.setName(name);
		doctor.setLocation(location);
		doctor.setSpecialization(speciality);
		doctor.setGender(gender);
		doctor.setAge(age);
		IDoctor ref=new DoctorImpl();
		ref.addDoctor(doctor);
		out.println("<html><body><script>alert('New Doctor Added')</script></body></html>");
		RequestDispatcher rd=request.getRequestDispatcher("admin.jsp");
		rd.include(request, response);
		
		
	}

}
