package com.app.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.bean.Doctor;
import com.app.bean.Patient;
import com.app.service.DoctorImpl;
import com.app.service.IDoctor;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

/**
 * Servlet implementation class AddPatientServlet
 */
@WebServlet("/addPatientServlet")
public class AddPatientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPatientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("name");
		
		String location=request.getParameter("city");
		long mobile=Long.parseLong(request.getParameter("mobile"));
		int age=Integer.parseInt(request.getParameter("age"));
		String gender=request.getParameter("gender");
		String password=request.getParameter("password");
		//int id=Integer.parseInt(request.getParameter("id"));
		
		Patient patient=new Patient();
		
		//patient.setPatientID(id);
		patient.setName(name);
		patient.setLocation(location);
	    patient.setMobile(mobile);
		patient.setGender(gender);
		patient.setPassword(password);
		patient.setAge(age);
		IPatient ref=new PatientImpl();
		ref.addPatient(patient);
			}

}
