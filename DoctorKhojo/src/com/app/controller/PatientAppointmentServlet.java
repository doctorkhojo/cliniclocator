package com.app.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.app.bean.Appointment;
import com.app.bean.Patient;
import com.app.dao.IPatientDAO;
import com.app.service.IPatient;
import com.app.service.PatientImpl;

import sun.misc.Request;

/**
 * Servlet implementation class PatientAppointmentServlet
 */
@WebServlet("/patientAppointmentServlet")
public class PatientAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientAppointmentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Patient patient = (Patient)session.getAttribute("myname");
		IPatient patientImpl = new PatientImpl();
		ArrayList<Appointment> list = patientImpl.getFromBookAppointment(patient.getName());
		request.setAttribute("bookingHistory", list);
		RequestDispatcher rd=request.getRequestDispatcher("viewHistory.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
