/**
 * 
 */
package com.app.bean;

/**
 * @author amenon
 *
 */
public class Patient {

	String name;
	int age;
	long mobile;
	String location;
	String gender;
	int patientID;
	String password;
	
	public Patient() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getPatientID() {
		return patientID;
	}

	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Patient [name=" + name + ", age=" + age + ", mobile=" + mobile + ", location=" + location + ", gender="
				+ gender + ", patientID=" + patientID + "]";
	}

	
	
}
