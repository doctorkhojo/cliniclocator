/**
 * 
 */
package com.app.bean;

/**
 * @author amang3
 *
 */
public class Appointment {
	String patientName,doctorName,day,time;
	int bookingId;
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getBookingId() {
		return bookingId;
	}
	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
	@Override
	public String toString() {
		return "PatientName="+patientName+"DoctorName=" + doctorName + " Day=" + day + " Time=" + time + "BookingId=" + bookingId;
	}
	
	

}
