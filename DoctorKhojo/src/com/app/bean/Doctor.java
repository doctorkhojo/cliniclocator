/**
 * 
 */
package com.app.bean;

/**
 * @author amenon
 *
 */
public class Doctor {
	

	String name;
	String specialization;
	String gender;
	String location;
	int age;
	int docID;
	
	
	public Doctor() {
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSpecialization() {
		return specialization;
	}


	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
	
	public int getDocID() {
		return docID;
	}


	public void setDocID(int docID) {
		this.docID = docID;
	}


	@Override
	public String toString() {
		return "Doctor [name=" + name + ", specialization=" + specialization + ", gender=" + gender + ", location="
				+ location + ", age=" + age + ", docID=" + docID + "]";
	}



	
}
