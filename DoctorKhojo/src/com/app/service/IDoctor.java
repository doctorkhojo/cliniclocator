package com.app.service;

import java.util.ArrayList;
import java.util.List;

import com.app.bean.Doctor;

/**
 * @author amenon
 *
 */
public interface IDoctor {

	void addDoctor(Doctor doctor);

	void removeDoctor(int docID);

	void updateDoctor(String specialization, String location, int docID);

	List<Doctor> getDoctor(String location, String specialization);

	Doctor getDoctorId(int docID);

	void updateAvailability(int docID, String time, String day);

	ArrayList<Doctor> getAllFromDoctor();
}
