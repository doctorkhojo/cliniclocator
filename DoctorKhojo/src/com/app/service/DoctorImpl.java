/**
 * 
 */
package com.app.service;

import java.util.ArrayList;
import java.util.List;

import com.app.bean.Doctor;
import com.app.dao.DoctorDAOImpl;
import com.app.dao.IDoctorDAO;

/**
 * @author amenon
 *
 */
public class DoctorImpl implements IDoctor {

	@Override
	public void addDoctor(Doctor doctor) {
		// TODO Auto-generated method stub
		IDoctorDAO doctorDAOimpl = new DoctorDAOImpl();
		doctorDAOimpl.addToDoctorDB(doctor);
	}

	@Override
	public void removeDoctor(int docID) {
		// TODO Auto-generated method stub
		IDoctorDAO doctorDAOimpl = new DoctorDAOImpl();
		doctorDAOimpl.removeFromDoctorDB(docID);
	}

	@Override
	public void updateDoctor(String specialization, String location, int docID) {
		// TODO Auto-generated method stub

		IDoctorDAO doctorDAOimpl = new DoctorDAOImpl();
		doctorDAOimpl.updateToDoctorDB(specialization, location, docID);
	}

	@Override
	public ArrayList<Doctor> getDoctor(String location, String specialization) {
		// TODO Auto-generated method stub
		IDoctorDAO doctorDAOimpl = new DoctorDAOImpl();
		ArrayList<Doctor> doclist = doctorDAOimpl.getFromDoctorDB(location, specialization);

		// doctorDAOimpl.getFromDoctorDB(location,specialization);
		return doclist;
	}

	@Override
	public Doctor getDoctorId(int docID) {
		IDoctorDAO doctorDAOImpl = new DoctorDAOImpl();
		return doctorDAOImpl.getFromDoctorIdDB(docID);
	}

	@Override
	public void updateAvailability(int docID, String time, String day) {
		// TODO Auto-generated method stub
		IDoctorDAO doctorDAOImpl = new DoctorDAOImpl();
		doctorDAOImpl.updateAvailabilityToDB(docID, time, day);

	}

	@Override
	public ArrayList<Doctor> getAllFromDoctor() {
		IDoctorDAO doctorDAOimpl = new DoctorDAOImpl();
		ArrayList<Doctor> doclist = doctorDAOimpl.getAllFromDoctorDB();
		return doclist;
	}
}