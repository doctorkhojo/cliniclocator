
package com.app.service;

import java.util.ArrayList;

import com.app.bean.Appointment;
import com.app.bean.Patient;

/**
 * @author amenon
 *
 */
public interface IPatient {

	Patient validatePatient(String name, String password);

	void addPatient(Patient patient);

	boolean bookAppointment(String patientName, String doctorName, String time, String day);

	boolean checkAvailabilityInDB(int docID, String time, String day);

	ArrayList<Appointment> getFromBookAppointment(String patientName);
	
	ArrayList<Patient> getAllPatientDB();
}
