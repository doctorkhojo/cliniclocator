/**
 * 
 */
package com.app.service;

import java.util.ArrayList;

import com.app.bean.Appointment;
import com.app.bean.Patient;
import com.app.dao.IPatientDAO;
import com.app.dao.PatientDAOImpl;

/**
 * @author amenon
 *
 */
public class PatientImpl implements IPatient {

		@Override
		public Patient validatePatient(String name, String password) {
			// TODO Auto-generated method stub
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			Patient patient= patientDAOImpl. authenticatePatient(name,password);
			return patient;
		}

		@Override
		public void addPatient(Patient patient) {

			// TODO Auto-generated method stub
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			patientDAOImpl.addToPatientDB(patient);
		}

		

		@Override
		public boolean checkAvailabilityInDB(int docID, String time, String day) {
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			return patientDAOImpl.checkAvailabilityInDB(docID, time, day);
		}

		@Override
		public boolean bookAppointment(String patientName, String doctorName, String time, String day) {
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			return patientDAOImpl.bookAppointmentToDB(patientName, doctorName, time, day);
		}

		@Override
		public ArrayList<Appointment> getFromBookAppointment(String patientName) {
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			return patientDAOImpl.getFromBookAppointmentToDB(patientName);
		}

		@Override
		public ArrayList<Patient> getAllPatientDB() {
			IPatientDAO patientDAOImpl = new PatientDAOImpl();
			return patientDAOImpl.getFromPatientDB();
		}
		
		

		
	}


