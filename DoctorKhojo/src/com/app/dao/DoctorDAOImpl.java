/**
 * 
 */
package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.app.bean.Doctor;

/**
 * @author tbanka
 *
 */
public class DoctorDAOImpl implements IDoctorDAO {

	Connection connection;
	String query;

	@Override
	public ArrayList<Doctor> getFromDoctorDB(String location, String specialization) {
		ResultSet rs = null;

		query = "Select * from doctor where location=? and speciality=?";
		connection = DBConnect.openConnection();
		ArrayList<Doctor> list = new ArrayList<Doctor>();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setString(1, location);
			p1.setString(2, specialization);
			rs = p1.executeQuery();

			while (rs.next()) {

				int docID = rs.getInt(6);
				String name = rs.getString(1);
				int age = rs.getInt(4);
				String gender = rs.getString(5);

				Doctor doctor = new Doctor();
				doctor.setAge(age);
				doctor.setDocID(docID);
				doctor.setGender(gender);
				doctor.setLocation(location);
				doctor.setName(name);
				doctor.setSpecialization(specialization);
				list.add(doctor);

			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public void addToDoctorDB(Doctor doctor) {
		// TODO Auto-generated method stub
		PreparedStatement statement = null;

		query = "Insert into doctor VALUES(?,?,?,?,?,doctor_seq.NextVal)";

		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement(query);

			statement.setString(1, doctor.getName());
			statement.setString(2, doctor.getSpecialization());
			System.out.println(doctor.getName());
			statement.setString(3, doctor.getLocation());
			statement.setInt(4, doctor.getAge());
			statement.setString(5, doctor.getGender());

			statement.execute();

			System.out.println("Doctor inserted");
		} catch (SQLException e) {
			System.out.println("SQLException");
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			try {
				DBConnect.closeConnection(connection);
				statement.close();

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void removeFromDoctorDB(int docID) {

		query = "Delete from doctor where ID=?";
		connection = DBConnect.openConnection();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setInt(1, docID);

			p1.execute();

			System.out.println("Doctor Removed id" + docID);
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	@Override
	public void updateToDoctorDB(String specialization, String location, int docID) {
		// TODO Auto-generated method stub

		query = "update doctor set speciality=?, location=? where ID=?";
		connection = DBConnect.openConnection();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setString(1, specialization);
			p1.setString(2, location);
			p1.setInt(3, docID);

			p1.execute();

			System.out.println("Doctor Updated with id: " + docID + " and specialization: " + specialization
					+ " and location: " + location);
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	@Override
	public Doctor getFromDoctorIdDB(int docID) {
		ResultSet rs = null;
		Doctor doctor = null;
		query = "Select * from doctor where id=?";
		connection = DBConnect.openConnection();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setInt(1, docID);
			rs = p1.executeQuery();

			while (rs.next()) {

				String name = rs.getString(1);
				String location = rs.getString(3);
				String specialization = rs.getString(2);
				int age = rs.getInt(4);
				String gender = rs.getString(5);

				doctor = new Doctor();
				doctor.setAge(age);
				doctor.setDocID(docID);
				doctor.setGender(gender);
				doctor.setLocation(location);
				doctor.setName(name);
				doctor.setSpecialization(specialization);

			}
			return doctor;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public void updateAvailabilityToDB(int docID, String time, String day) {
		PreparedStatement statement = null;

		query = "Insert into slots VALUES(?,?,?)";

		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement(query);

			statement.setInt(1, docID);
			statement.setString(2, day);
			statement.setString(3, time);

			statement.execute();

			System.out.println("Slots inserted");
		} catch (SQLException e) {
			System.out.println("SQLException");
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			try {
				DBConnect.closeConnection(connection);
				statement.close();

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	@Override
	public ArrayList<Doctor> getAllFromDoctorDB() {
		ResultSet rs = null;

		query = "Select * from doctor";
		connection = DBConnect.openConnection();
		ArrayList<Doctor> list = new ArrayList<Doctor>();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			rs = p1.executeQuery();

			while (rs.next()) {

				String specialization = rs.getString(2);
				String location = rs.getString(3);
				int docID = rs.getInt(6);
				String name = rs.getString(1);
				int age = rs.getInt(4);
				String gender = rs.getString(5);

				Doctor doctor = new Doctor();
				doctor.setAge(age);
				doctor.setDocID(docID);
				doctor.setGender(gender);
				doctor.setLocation(location);
				doctor.setName(name);
				doctor.setSpecialization(specialization);
				list.add(doctor);

			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

}
