/**
 * 
 */
package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.app.bean.Appointment;
import com.app.bean.Patient;

/**
 * @author tbanka
 *
 */
public class PatientDAOImpl implements IPatientDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.IPatientDAO#getPatient()
	 */
	Connection connection;
	String query;

	@Override
	public ArrayList<Patient> getFromPatientDB() {
		ResultSet rs = null;

		query = "Select * from patient";
		connection = DBConnect.openConnection();
		ArrayList<Patient> list = new ArrayList<Patient>();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			rs = p1.executeQuery();

			while (rs.next()) {

				int patientID = rs.getInt(6);
				String password = rs.getString(7);
				String name = rs.getString(1);
				int age = rs.getInt(2);
				String gender = rs.getString(3);
				String location = rs.getString(4);
				Long mobile = rs.getLong(5);

				Patient patient = new Patient();
				patient.setPassword(password);
				patient.setPatientID(patientID);
				patient.setName(name);
				patient.setAge(age);
				patient.setGender(gender);
				patient.setLocation(location);
				patient.setMobile(mobile);
				list.add(patient);

			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public boolean addToPatientDB(Patient patient) {
		PreparedStatement statement = null;

		query = "Insert into patient VALUES(?,?,?,?,?,patient_seq.NextVal,?)";

		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement(query);

			statement.setString(1, patient.getName());
			statement.setInt(2, patient.getAge());
			statement.setString(3, patient.getGender());
			statement.setString(4, patient.getLocation());
			statement.setLong(5, patient.getMobile());
			statement.setString(6, patient.getPassword());

			statement.execute();

			System.out.println("in patient insert");
			return true;
		} catch (SQLException e) {
			System.out.println("SQLException");
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			try {
				DBConnect.closeConnection(connection);
				statement.close();

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return false;
	}

	@Override
	public Patient authenticatePatient(String name, String password) {
		ResultSet rs = null;

		query = "Select * from patient where name=? and password=?";
		connection = DBConnect.openConnection();

		Patient patient = null;

		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setString(1, name);
			p1.setString(2, password);
			rs = p1.executeQuery();

			while (rs.next()) {

				int patientID = rs.getInt(6);
				String pname = rs.getString(1);
				int age = rs.getInt(2);
				String gender = rs.getString(3);
				String location = rs.getString(4);
				Long mobile = rs.getLong(5);

				patient = new Patient();
				patient.setPatientID(patientID);
				patient.setName(pname);
				patient.setAge(age);
				patient.setGender(gender);
				patient.setLocation(location);
				patient.setMobile(mobile);
			}
			return patient;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public boolean checkAvailabilityInDB(int docID, String time, String day) {
		ResultSet rs = null;
		System.out.println(time);
		System.out.println(docID);
		System.out.println(day);
		query = "Select * from slots where id=? and day=? and time=?";
		connection = DBConnect.openConnection();

		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setInt(1, docID);
			p1.setString(2, day);
			p1.setString(3, time);
			rs = p1.executeQuery();

			if (rs.next()) {
				System.out.println("DB");
				return false;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println("DBasas");
		return true;
	}

	@Override
	public boolean bookAppointmentToDB(String patientName, String doctorName, String time, String day) {
		PreparedStatement statement = null;
		boolean flag = false;
		query = "Insert into appointment VALUES(?,?,?,?,appointment_seq.NextVal)";

		try {
			connection = DBConnect.openConnection();
			statement = connection.prepareStatement(query);

			statement.setString(1, patientName);
			statement.setString(2, doctorName);
			statement.setString(3, day);
			statement.setString(4, time);

			flag = statement.execute();

			System.out.println("Slots inserted");
			return flag;
		} catch (SQLException e) {
			System.out.println("SQLException");
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			try {
				DBConnect.closeConnection(connection);
				statement.close();

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return flag;
	}

	@Override
	public ArrayList<Appointment> getFromBookAppointmentToDB(String patientName) {
		ResultSet rs = null;

		query = "Select * from appointment where patient=?";
		connection = DBConnect.openConnection();
		ArrayList<Appointment> list = new ArrayList<Appointment>();
		PreparedStatement p1 = null;
		try {
			p1 = connection.prepareStatement(query);
			p1.setString(1, patientName);
			rs = p1.executeQuery();

			while (rs.next()) {

				String doctorName = rs.getString(2);
				String day = rs.getString(3);
				String time = rs.getString(4);
				int bookingId = rs.getInt(5);

				Appointment appointment = new Appointment();
				appointment.setPatientName(patientName);
				appointment.setDoctorName(doctorName);
				appointment.setTime(time);
				appointment.setDay(day);
				appointment.setBookingId(bookingId);
				list.add(appointment);

			}
			return list;
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			try {
				p1.close();
				DBConnect.closeConnection(connection);

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

}
