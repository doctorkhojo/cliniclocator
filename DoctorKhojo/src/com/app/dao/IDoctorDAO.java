/**
 * 
 */
package com.app.dao;

import java.util.ArrayList;


import com.app.bean.Doctor;

/**
 * @author amang3
 *
 */
public interface IDoctorDAO {

	ArrayList<Doctor> getFromDoctorDB(String location, String specialization);
	
	void addToDoctorDB(Doctor doctor);
	
	void removeFromDoctorDB(int docID);
	
	void updateToDoctorDB(String specialization, String location, int docID);
	
	Doctor getFromDoctorIdDB(int docID);
	
	 void updateAvailabilityToDB(int docID, String time, String day);
	 
	 ArrayList<Doctor> getAllFromDoctorDB();
	
}
