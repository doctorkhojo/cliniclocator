/**
 * 
 */
package com.app.dao;


import java.util.ArrayList;

import com.app.bean.Appointment;
import com.app.bean.Patient;

/**
 * @author amang3
 *
 */
public interface IPatientDAO {

	ArrayList<Patient> getFromPatientDB();
	
	boolean addToPatientDB(Patient patient);
	
	Patient authenticatePatient(String name, String password);
	
	public boolean checkAvailabilityInDB(int docID, String time, String day);
	
	public boolean bookAppointmentToDB(String patientName, String doctorName, String time, String day);
	
	ArrayList<Appointment> getFromBookAppointmentToDB(String patientName);
}
