/**
 * 
 */
package com.app.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author amang3
 *
 */
public class DBConnect {

	static Connection con;

	public	static Connection openConnection() {
			
			String driverName = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:xe"; //"jdbc:oracle:thin:@10.150.222.26:1521:xe";
			String username = "ashish";
			String password = "ashish";
			try {
				Class.forName(driverName);
				con = DriverManager.getConnection(url, username, password);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			return con;
		}

		static void closeConnection(Connection con) {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				//e.printStackTrace();
			}
		}
}
