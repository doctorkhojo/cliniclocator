<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<style type="text/css">

#form {
	position: absolute;
	left: 50%;
	top: 35%
}
#table {
	position: absolute;
	left: 10%;
	top: 20%
}

#h3 {
	position: absolute;
	left: 33%;
	top: 33%;
	color: black;
}

#p {
	position: absolute;
	left: 12%;
	top: 7%;
	color: black;
}

#hr {
	position: relative;
	right: 21%;
}

#button {
	background-color: #D3D3D3; /* Green */
	border: none;
	color: white;
	padding: 8px 30px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
}table#id {
   font-size: 20px;
   color:black;
}

#id1{
position: absolute;
       left: 50%;
       top:5%;
font-size:20px;
color:black
}
#link{
font-size: 15px;
}
}

</style>
       <div
              style="background: #20B2AA; height: 130px; border-radius: 25px; border: 2px solid black;">
              <span style="position: absolute; left: 107px;top: 10px"><h1> DoctorKhojo</h1></span>
                     
              <span style="color:white;position: absolute; left: 80px;top: 50px"><h3>Your Online Healthcare Guide</h3></span>
                                  
                           <span
                     style="position: absolute; left:1100px; top: 30px">
                     
                     <c:choose>
  <c:when test="${sessionScope.myname != null}">
    <pre id="id1">Welcome,<c:out value="${sessionScope.myname.name}"/>
   <p id="link"><a href="viewProfile.jsp">View Your Profile</a> | <a href="logoutSession">Logout</a></p></pre>
  </c:when>
  <c:otherwise>

                     <form action="loginServlet" method="post">
                     <table id="id">

				
					<tr>
						<td><strong>Username:</strong></td>
						<td><input type="text" name="name"></td>
					</tr>
					<tr>
						<td><strong>Password:</strong></td>
						<td><input type="password" name="password"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="LOGIN"> <a
							href="welcome.jsp"> Sign Up</a></td>
					</tr>
				
			</table>
			</form> </c:otherwise>
</c:choose></span>
	</div>
	<br>
	<div style="background: #D3D3D3; height: 550px; border-radius: 25px;">
		<div id="h3"
			style="background: #20B2AA; height: 300px; width: 450px; border-radius: 25px;">
			<table id="table">
			<tr><td>Name:</td>
			<td>${mydoc.name}</td></tr>
			<tr><td>Age:</td>
			<td>${mydoc.age}</td></tr>
			<tr><td>Gender:</td>
			<td>${mydoc.gender}</td></tr>
			<tr><td>Specialty:</td>
			<td>${mydoc.specialization}</td></tr>
			<tr><td>Location:</td>
			<td>${mydoc.location}</td></tr>
			
			</table>
			
			<form id="form" action="availabilityservlet" method="post">
			<input type="hidden" name="doctorid" value="${mydoc.docID }">
			<input type="hidden" name="doctorname" value="${mydoc.name }">
				------Day--------Time----<br>
				<select name="day">
				<option value="Sunday">Sunday</option>
				<option value="Monday">Monday</option>
				<option value="Tuesday">Tuesday</option>
				<option value="Wednesday">Wednesday</option>
				<option value="Thursday">Thursday</option>
				<option value="Friday">Friday</option>
				<option value="Saturday">Saturday</option>
				</select>
				
				<select name="time">
				<option value="8am">8 am</option>
				<option value="9am">9 am</option>
				<option value="10am">10 am</option>
				<option value="5pm">5 pm</option>
				<option value="6pm">6 pm</option>
				<option value="7pm">7 pm</option>
				<option value="8pm">8 pm</option>
				</select><br>

				<input id="button" type="submit" value="Get Appointment">
			</form>
		</div>
	</div>
</body>
</html>